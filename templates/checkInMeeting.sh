#!/bin/bash

hassUrl="{{ hass_url }}"
hassToken="{{ hass_token }}"
hassSensor="{{ hass_entity_id }}"

fuser -s /dev/video*
videoOn=$?

audioSources=$(pactl list source-outputs)

delim="

"
string="$audioSources$delim"

array=()
while [[ "$string" ]]; do
  array+=( "${string%%$delim*}"  )
  string="${string#*$delim}"
done

micOn=1

for i in "${array[@]}"; do
  grep -q "media.role = \"phone\"" <<< "$i"
  if [ $? -eq 0 ]; then
    grep -q "Corked: no" <<< "$i"
    if [ $? -eq 0 ]; then
      micOn=0
    fi
  fi
done

if [ $videoOn -eq 0 ] || [ $micOn -eq 0 ]; then
  curl -X POST -H "Authorization: Bearer $hassToken" \
    -H "Content-Type: application/json" \
    -d '{"state": "on", "attributes": {"friendly_name": "{{ hass_entity_name }}"}}' \
    $hassUrl/api/states/binary_sensor.$hassSensor > /dev/null
else
  curl -X POST -H "Authorization: Bearer $hassToken" \
    -H "Content-Type: application/json" \
    -d '{"state": "off", "attributes": {"friendly_name": "{{ hass_entity_name }}"}}' \
    $hassUrl/api/states/binary_sensor.$hassSensor > /dev/null
fi
