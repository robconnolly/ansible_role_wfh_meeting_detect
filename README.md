Ansible Role to Deploy WFH Meeting Detection
=========

Deploy work from home (WFH) meeting detection scripts to a given machine.
As per
[https://medium.com/@hansenji/linux-triggers-for-in-meeting-indicator-ea06cdbe41bd](https://medium.com/@hansenji/linux-triggers-for-in-meeting-indicator-ea06cdbe41bd),
but with Home Assistant sensor integration.

Requirements
------------

None

Role Variables
--------------

The following variables are available:

* `hass_url` - URL of your Home Assistant server
* `hass_token` - Long lived access token for Home Assistant
* `hass_entity_id` - HASS binary sensor entity ID (without the entity type)
* `hass_entity_name` - Friendly name for the binary sensor

Dependencies
------------

None.

Example Playbook
----------------

Use the role as follows:

```yaml
  - hosts: work_machine
    vars:
      hass_url: "https://hassio.local"
      hass_token: "blahblahblah"
    roles:
      - role: wfh_meeting_detect
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
